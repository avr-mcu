// Copyright (C)2019, Philip Munts, President, Munts AM Corp.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef _gpio_h_
#define _gpio_h_

#include <sbit.h>

#define DDRA_0		SBIT(DDRA, 0)
#define DDRA_1		SBIT(DDRA, 1)
#define DDRA_2		SBIT(DDRA, 2)
#define DDRA_3		SBIT(DDRA, 3)
#define DDRA_4		SBIT(DDRA, 4)
#define DDRA_5		SBIT(DDRA, 5)
#define DDRA_6		SBIT(DDRA, 6)
#define DDRA_7		SBIT(DDRA, 7)

#define PORTA_0		SBIT(PORTA, 0)
#define PORTA_1		SBIT(PORTA, 1)
#define PORTA_2		SBIT(PORTA, 2)
#define PORTA_3		SBIT(PORTA, 3)
#define PORTA_4		SBIT(PORTA, 4)
#define PORTA_5		SBIT(PORTA, 5)
#define PORTA_6		SBIT(PORTA, 6)
#define PORTA_7		SBIT(PORTA, 7)

#define DDRB_0		SBIT(DDRB, 0)
#define DDRB_1		SBIT(DDRB, 1)
#define DDRB_2		SBIT(DDRB, 2)
#define DDRB_3		SBIT(DDRB, 3)
#define DDRB_4		SBIT(DDRB, 4)
#define DDRB_5		SBIT(DDRB, 5)
#define DDRB_6		SBIT(DDRB, 6)
#define DDRB_7		SBIT(DDRB, 7)

#define PORTB_0		SBIT(PORTB, 0)
#define PORTB_1		SBIT(PORTB, 1)
#define PORTB_2		SBIT(PORTB, 2)
#define PORTB_3		SBIT(PORTB, 3)
#define PORTB_4		SBIT(PORTB, 4)
#define PORTB_5		SBIT(PORTB, 5)
#define PORTB_6		SBIT(PORTB, 6)
#define PORTB_7		SBIT(PORTB, 7)

#define DDRC_0		SBIT(DDRC, 0)
#define DDRC_1		SBIT(DDRC, 1)
#define DDRC_2		SBIT(DDRC, 2)
#define DDRC_3		SBIT(DDRC, 3)
#define DDRC_4		SBIT(DDRC, 4)
#define DDRC_5		SBIT(DDRC, 5)
#define DDRC_6		SBIT(DDRC, 6)
#define DDRC_7		SBIT(DDRC, 7)

#define PORTC_0		SBIT(PORTC, 0)
#define PORTC_1		SBIT(PORTC, 1)
#define PORTC_2		SBIT(PORTC, 2)
#define PORTC_3		SBIT(PORTC, 3)
#define PORTC_4		SBIT(PORTC, 4)
#define PORTC_5		SBIT(PORTC, 5)
#define PORTC_6		SBIT(PORTC, 6)
#define PORTC_7		SBIT(PORTC, 7)

#define DDRD_0		SBIT(DDRD, 0)
#define DDRD_1		SBIT(DDRD, 1)
#define DDRD_2		SBIT(DDRD, 2)
#define DDRD_3		SBIT(DDRD, 3)
#define DDRD_4		SBIT(DDRD, 4)
#define DDRD_5		SBIT(DDRD, 5)
#define DDRD_6		SBIT(DDRD, 6)
#define DDRD_7		SBIT(DDRD, 7)

#define PORTD_0		SBIT(PORTD, 0)
#define PORTD_1		SBIT(PORTD, 1)
#define PORTD_2		SBIT(PORTD, 2)
#define PORTD_3		SBIT(PORTD, 3)
#define PORTD_4		SBIT(PORTD, 4)
#define PORTD_5		SBIT(PORTD, 5)
#define PORTD_6		SBIT(PORTD, 6)
#define PORTD_7		SBIT(PORTD, 7)

#endif

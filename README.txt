               AVR Microcontroller Firmware Development Framework

   Here is a framework for developing firmware for [1]AVR single chip
   microcontrollers.

News

     * 1 January 2015 -- Upgraded binutils to 2.25 and gcc to 4.8.4.
     * 21 January 2015 -- Cleaned up the toolchain makefile. Build gcc
       prerequisites, including libiconv, from scratch.
     * 5 August 2015 -- Upgraded gcc to 4.9.3.
     * 22 January 2016 -- Upgraded gcc to 5.3.0.
     * 6 April 2016 -- New toolchain release 2016.097: Upgraded binutils
       to 2.26. Upgraded avr-libc to 2.0.0.
     * 21 November 2016 -- Upgraded binutils to 2.27 and gcc to 6.2.0.
     * 12 March 2017 -- Moved the original GCC framework into the gcc/
       subdirectory.
     * 17 March 2017 -- Upgraded binutils to 2.28 and gcc to 6.3.0.
       Imported [2]LEGO Power Functions Infrared Remote Control Protocol
       implementation.
     * 19 September 2017 -- Upgraded binutils to 2.29 and gcc to 7.2.0.
     * 16 March 2019 -- Added make files and a demo program using
       [3]mikroPascal PRO for AVR.
     * 18 March 2019 -- Upgraded binutils to 2.32 and gcc to 8.3.0. Enable
       C++ when building the toolchain. Added sbit.h and gpio.h for bit
       operations. Imported libstream. Added an LED test demo program,
       which works on the Arduino Uno Rev 3 and similar boards with the
       LED connected to PB5.

   Note: I have largely migrated to [4]ARM microcontrollers, so this AVR
   framework will only be updated sporadically from now on.

Git Repository

   The source code is available at: [5]http://git.munts.com/avr-mcu

   Use the following command to clone it:

   git clone http://git.munts.com/avr-mcu.git
   _______________________________________________________________________

   Questions or comments to Philip Munts [6]phil@munts.net

   I am available for custom system development (hardware and software) of
   products using AVR or other microcontrollers.

References

   1. http://www.atmel.com/products/microcontrollers/avr/default.aspx
   2. https://www.lego.com/en-us/powerfunctions/articles/8884-control-5fcb2efbb4e74f0c926948df71445765
   3. https://www.mikroe.com/mikropascal-avr
   4. http://git.munts.com/arm-mcu
   5. http://git.munts.com/avr-mcu
   6. mailto:phil@munts.net
